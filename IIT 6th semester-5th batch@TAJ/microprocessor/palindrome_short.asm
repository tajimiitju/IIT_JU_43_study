    .data
    prompt1 db "Palindrome.$"
    prompt2 db "Not Palindrome.$"
    string1 db "ssaadaass"       ;the string to be checked
    size dw 9                    ;size of the string

    
    .code
    main proc
    lea si, string1
    mov di, si
    add di, size
    dec di
    
    
    mov ax, size
    mov bx, 2
    mov dx, 0
    div bx
    mov cx, ax                   ;cx contains the number of iteration
    
    
    check:
    mov dh, [si]
    mov dl, [di]
    cmp dh, dl
    jne n_p
    inc si
    dec di
    loop check
    
    lea dx, prompt1
    mov ah, 9
    int 21h
    
    jmp end
    
    n_p:
    
    lea dx, prompt2
    mov ah, 9
    int 21h
    
    end:        
    
    
    ; wait for any key....    
    mov ah, 1
    int 21h
    
    main endp
    
    mov ax, 4c00h ; exit to operating system.
    int 21h
    
    end main     
