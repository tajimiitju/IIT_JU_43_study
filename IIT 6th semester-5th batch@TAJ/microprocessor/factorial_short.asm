.model small
.stack 100h

.data
    
    fact dw 1d                    


.code

main proc
    
    mov cx, 10       ;cx contains the number of which factorial to be calculated
    mov bx, 1
    mov ax, 1
    
    factorial:
    mov dx, 0
    mul bx
    inc bx                                                                                     
    loop factorial
    
    mov fact, ax
    
    push 99
    mov bx, 10
    mov ax, fact 
    
    insert_stack:
    cmp ax, 0
    je end_insert
    mov dx, 0
    div bx
    push dx
    jmp insert_stack
    end_insert:    
    
    output:
    pop dx
    cmp dx, 99
    je end_output
    add dx, 48
    mov ah,2
    int 21h
    jmp output
    end_output:
            
    ; wait for any key....    
    mov ah, 1
    int 21h
    
main endp

    mov ax, 4c00h ; exit to operating system.
    int 21h    
end main
