; multi-segment executable file template.

data segment
    ; add your data here!
    pkey db "press any key...$"
    prompt1 db "Palindrome.$"
    prompt2 db "Not Palindrome.$"
    string1 db 256 dup('$')
ends

stack segment
    dw   128  dup(0)
ends

code segment
start:
; set segment registers:
    mov ax, data
    mov ds, ax
    mov es, ax
    
    

    ; add your code here
    
    lea di, string1
    mov si, di
    
    input:
    mov ah, 1 
    int 21h        
    cmp al, 13
    je input_end
    mov byte ptr [di], al
    inc di
    jmp input
    
    input_end:
    dec di
    
    mov ax, di
    sub ax, si
    inc ax
    mov bx, 2
    mov dx, 0
    div bx
    
    mov cx, ax
    
    check:
    mov dh, byte ptr [si]
    mov dl, byte ptr [di]
    cmp dh, dl
    jne n_p
    inc si
    dec di
    loop check
    
    lea dx, prompt1
    mov ah, 9
    int 21h
    
    jmp end
    
    n_p:
    
    lea dx, prompt2
    mov ah, 9
    int 21h
    
    end:        
    lea dx, pkey
    mov ah, 9
    int 21h        ; output string at ds:dx
    
    ; wait for any key....    
    mov ah, 1
    int 21h
    
    mov ax, 4c00h ; exit to operating system.
    int 21h    
ends

end start ; set entry point and stop the assembler.
