; a program to print first ten numbers by fibonacci series

.MODEL SMALL
.STACK 100H
.DATA
	ms1 DB "First Ten Numbers Fibonacci Series is: $"
.CODE
	MAIN PROC
		MOV AX,@DATA
		MOV DS,AX
				
		MOV AH,09H
		LEA DX,ms1
		INT 21H
		
		MOV CX,10D
		
		MOV AX,1H
		MOV BX,0
		MOV SI,0
		LEVEL_A:
			
			ADD AX,BX
			
			PUSH AX
			PUSH CX
			PUSH BX
			
			CALL DISPLAY
			CALL SPACE
			
			POP BX
			POP CX
			POP AX
			
			MOV SI,0
			MOV SI,AX
			MOV AX,BX
			MOV BX,0
			MOV BX,SI
			
		LOOP LEVEL_A
		
		
		
		
		MOV AH,4CH
		INT 21H
	MAIN ENDP
			
	
	
	SPACE PROC			
			
			MOV DL,' '
			MOV AH,02H
			INT 21H
		
		RET
	SPACE ENDP			
	
	
	
	DISPLAY PROC				
		
		MOV DX,0H
		MOV CX,0H
		
		DIS_LEVEL_1:			
			MOV BX,10D
			DIV BX
			
			ADD DX,30H
			
			PUSH DX
			
			INC CX
			
			MOV DX,0H
			
			CMP AX,0H
			JE DIS_LEVEL_2
		
		JMP DIS_LEVEL_1
		
		DIS_LEVEL_2:
		
		MOV AX,0H
		
		LEVEL_3:
			POP AX
			MOV DL,AL
			MOV AH,02H
			INT 21H
			MOV AX,0H
		LOOP LEVEL_3
		
		RET
	DISPLAY ENDP						
	
	
	
END MAIN