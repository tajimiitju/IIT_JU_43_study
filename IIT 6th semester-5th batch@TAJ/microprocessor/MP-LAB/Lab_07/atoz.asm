.model small
.stack 100h
.data
.code

main proc

mov ah,2
mov cx,26
mov dl,'A'
	top:
	    int 21h
	    mov bl,dl
	    mov dl,' '
	    int 21h
	    mov dl,bl
	    inc dl
	 loop top

mov ah,4ch
int 21h

main endp
end main