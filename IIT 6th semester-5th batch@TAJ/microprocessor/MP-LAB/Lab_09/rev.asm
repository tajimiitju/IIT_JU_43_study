; a program which will read a string inputs and print out the string in reverse order

.MODEL SMALL
.STACK 100H

	
.CODE
	MAIN PROC
		
		MOV AH,2
		MOV DL,'?'
		INT 21h
		XOR CX,CX
		MOV AH,1
		INT 21h

While_:

	CMP AL,0dh ; carraige return
	je END_WHILE

	Push AX
	INC  CX

;read a character

	INT 21h

	JMP While_ ;loop back

END_WHILE:

; go to a new line

		MOV AH,2
		MOV DL,0dh
		INT 21h
		Mov DL,0ah
		INT 21h
		JCXZ EXIT

 TOP :
 
 ;pop a character from the stack

     		POP DX
 ;display it

		INT 21h
		LOOP TOP

 EXIT:

		MOV AH,4CH
 		INT 21h

Main Endp

	END MAIN