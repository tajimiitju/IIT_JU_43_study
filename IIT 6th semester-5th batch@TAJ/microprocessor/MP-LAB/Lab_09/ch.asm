; Develop and execute and assembly language program to convert a lowercase letter to an uppercase letter and vice versa

.MODEL SMALL
.STACK 100H
.DATa
	MS1 DB "1. To Convert Lowercase To Uppercase $"
	MS2 DB "2. To Convert Uppercase to Lowercase $"
	MS3 DB "Enter Your Choose: $"
	MS4 DB "Enter Lowercase Letter: $"
	MS5 DB "Enter Uppercase Letter: $"
	MS6 DB "Lowercase Letter is: $"
	MS7 DB "Uppercase Letter is: $"
	MS8 DB "Wrong Option $"
	
.CODE
	MAIN PROC
		MOV AX,@DATA
		MOV DS,AX
		
		MOV AH,09H
		LEA DX,MS1
		INT 21H
		
		CALL NEWLINE
		
		MOV AH,09H		
		LEA DX,MS2
		INT 21H
		
		CALL NEWLINE
		CALL NEWLINE

		MOV AH,09H		
		LEA DX,MS3
		INT 21H
		
		MOV AH,01H
		INT 21H
		
		CMP AL,'1'
			JE LTOU
		CMP AL,'2'
			JE UTOL
		JMP ERROR_LEVEL
			
		LTOU:
			CALL NEWLINE
			CALL NEWLINE
			
			MOV AH,09H
			LEA DX,MS4
			INT 21H
			
			MOV AH,01H
			INT 21H
			SUB AL,32D
			
			MOV CL,AL
			
			CALL NEWLINE
			
			MOV AH,09H
			LEA DX,MS7
			INT 21H
			
			MOV DL,CL
			MOV AH,02H
			INT 21H
			
			
			JMP END_LEVEL
				
		
		UTOL:
			CALL NEWLINE
			CALL NEWLINE
			
			MOV AH,09
			LEA DX,MS5
			INT 21H
			
			
			MOV AH,01H
			INT 21H
			ADD AL,32D
			
			MOV CL,AL
			
			CALL NEWLINE
			
			MOV AH,09H
			LEA DX,MS6
			INT 21H
			
			MOV DL,CL
			MOV AH,02H
			INT 21H
			
			JMP END_LEVEL
			
		ERROR_LEVEL:
			CALL NEWLINE
			MOV AH,09H
			LEA DX,MS8
			INT 21H
			
		END_LEVEL:
		
		
		MOV AH,4CH
		INT 21H
	MAIN ENDP
		
	
	
	NEWLINE PROC
		MOV AH,02H
		MOV DL,0AH
		INT 21H
		
		MOV DL,0DH
		INT 21H
		
		RET
	NEWLINE ENDP
	
END MAIN