.model small
.stack 100h
.code

main proc

mov cx,10
mov bx,48

print:
mov ah,2
mov dl,bl
int 21h

mov ah,2
mov dl,' '
int 21h

inc bx
dec cx
jnz print

mov ah,4ch
int 21h

main endp
end main

