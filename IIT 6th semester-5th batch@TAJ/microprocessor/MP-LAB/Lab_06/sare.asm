;this program is written for print the line with sound
;		SA	RE	GA	MA

.model small
.stack 100h
.data
.code

main proc

mov ah,2

mov dl,'S'
int 21h

mov dl,'A'
int 21h

mov dl,7h
int 21h 	; 1st bell
int 21h		; 2nd bell

mov dl,9h
int 21h		; vertical tab

mov dl,'R'
int 21h

mov dl,'E'
int 21h

mov dl,07h
int 21h
int 21h

mov dl,09h
int 21h

mov dl,'G'
int 21h

mov dl,'A'
int 21h

mov dl,07h
int 21h
int 21h

mov dl,09h
int 21h

mov dl,'M'
int 21h

mov dl,'A'
int 21h

mov dl,07h
int 21h
int 21h

mov dl,09h
int 21h

mov ah,4ch
int 21h

main endp
end main