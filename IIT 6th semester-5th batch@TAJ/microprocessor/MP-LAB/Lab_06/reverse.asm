.model small
.stack 100h
.data

msg db 'Enter five characters: $'
msg1 db 0ah,0dh, 'The reverse order is: $'
a db ?

.code

main proc

mov ax,@data
mov ds,ax

lea dx,msg
mov ah,09h
int 21h

mov ah,1

int 21h
mov a,al

int 21h
mov bh,al

int 21h
mov bl,al

int 21h
mov ch,al

int 21h
mov cl,al


lea dx,msg1
mov ah,09h
int 21h

mov ah,2

mov dl,cl
int 21h

mov dl,ch
int 21h

mov dl,bl
int 21h

mov dl,bh
int 21h

mov dl,a
int 21h

mov ah,4ch
int 21h

main endp
end main