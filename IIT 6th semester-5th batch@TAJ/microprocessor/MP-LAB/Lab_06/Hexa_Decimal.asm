;this is the program to read one of the hex digit A-F & display it on the 
;next line in decimal order(for example for a-10,b-11 such that)

.model small
.stack 100h
.data

msg  db 'ENTER A HEX NUMBER: $'
msg1 db 0ah,0dh, 'THE DECIMAL VALUE IS: $' 

.code

main proc

mov ax,@data
mov ds,ax
lea dx,msg
mov ah,9
int 21h

mov ah,1
int 21h
mov bl,al
sub bl,11h

lea dx,msg1
mov ah,9
int 21h

mov ah,2
mov dl,'1'
int 21h



mov dl,bl
int 21h

mov ah,4ch
int 21h

main endp
end main