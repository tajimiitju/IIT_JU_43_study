; this is a program to display a) " ? " b)Read two decimal digit whose sum is less
;than 10 c) Display them & their sum on the next line with an appropriate message.

.model small
.stack 100h
.data

msg  db '? $'
msg1 db 0dh,0ah, 'THE SUM OF $'
msg2 db 'AND $'
msg3 db 'IS $'

.code

main proc

mov ax,@data
mov ds,ax
lea dx,msg		;load message
mov ah,9
int 21h			;display message ?

mov ah,1		
int 21h			; input first character
mov bl,al		; store the input at bl
int 21h			; input second character
mov bh,al		; store the input at bh

lea dx,msg1		;load first message
mov ah,9
int 21h			;display first message "the sum of"
mov ah,2
mov dl,bl
int 21h			;display first digit
mov dl,' '
int 21h

lea dx,msg2		;load second message 
mov ah,9
int 21h			;display second message "AND"
mov ah,2
mov dl,bh
int 21h			;display second digit
mov dl,' '
int 21h

lea dx,msg3		;load third message 
mov ah,9
int 21h			;display third message "IS"

mov ah,2
add bl,bh		; add two digit
sub bl,30h		;convert it in decimal
mov dl,bl
int 21h			; display the summation of two digit

mov ah,4ch
int 21h			; dos exit

main endp
end main