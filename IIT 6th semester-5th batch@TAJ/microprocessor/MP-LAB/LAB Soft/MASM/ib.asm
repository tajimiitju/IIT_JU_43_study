TITLE PGM4_6: IBM CHARACTER DISPLAY

.MODEL SMALL
.STACK 100H
.CODE

MAIN PROC
     MOV AH,2        ; display character function
     MOV CX,256      ; no. of character display
     MOV DL,0        ; DL has ASCII code of null
PRINT_LOOP:
     INT 21H         ; display character
     INC DL          ; increment ASCII code
     DEC CX          ; decrement counter
     JNZ PRINT_LOOP  ; keep going if CX not 0

;RETURN TO DOS
        MOV AH,4CH        
        INT 21H           
MAIN ENDP
   END MAIN
