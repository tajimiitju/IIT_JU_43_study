TITLE PGM4_3: Case conversion (Lowercase to Uppercase)

.MODEL SMALL
.STACK 100H
.DATA
CR EQU 0DH
LF EQU 0AH
MSG1 DB 'ENTER A LOWERCASE LETER: $'
MSG2 DB 0DH,0AH,'IN UPPERCASE IT IS:'
CHAR DB ?,'$'
.CODE
MAIN PROC
;DISPLAY PROMPT 
         MOV AX,@DATA          
         MOV DS,AX        
;INPUT A CHARACTE
         LEA DX,MSG1 
         MOV AH,9          
         INT 21H           
;GO TO NEW LINE
        MOV AH,1     
        INT 21H            
        SUB AL,20H
        MOV CHAR,AL                 
;DISPLSY CHARACTER
        LEA DX,MSG2     
        MOV AH,9          
        INT 21H            
;RETURN TO DOS
        MOV AH,4CH         
        INT 21H            
MAIN ENDP
        END MAIN
