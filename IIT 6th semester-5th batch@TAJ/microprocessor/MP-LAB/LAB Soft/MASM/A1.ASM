TITLE PROG-1:PRINT STRING PROGRAM

.MODEL SMALL
.STACK 100H
.CODE
        MAIN PROC
        MOV DX,0
        MOV AH,1
        INT 21H

       WHILE_:

        CMP AL,0DH
        JE E
        INC DX
        INT 21H
        JMP WHILE_
        E:
        ADD DX,48
        MOV AH,2
        INT 21H

        MOV AH,4CH
        INT 21H

        MAIN ENDP
        END MAIN

