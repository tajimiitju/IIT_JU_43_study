.MODEL SMALL
.STACK 100H
.data

msg1 DB 0DH,0AH,'The sum of '
dig1 DB ?
msg2 DB ' AND '
dig2 DB ?
msg3 DB 'IS :'
dig  DB ?,'$'

.CODE

MAIN PROC
mov ax, @DATA
mov ds, ax

mov dl, '?'
mov ah,2
int 21h

;input
mov ah,1
int 21h
mov dig1, al
int 21h
mov dig2,al

;int 21h
;mov al,dig1

add al,dig1
sub al,30h
mov dig,al

lea dx,msg1
mov ah,9
int 21h

;dos exit

mov ah,4ch
int 21h

main endp
end main
