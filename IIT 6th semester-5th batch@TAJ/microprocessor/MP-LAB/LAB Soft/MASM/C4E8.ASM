TITLE CHATER:4 EXERCISE:8
.MODEL SMALL
.STACK 100H
.DATA

MSG1    DB  0DH,0AH,'THE SUM OF '
DIG1    DB ?
MSG2    DB ' AND '
DIG2    DB ?
MSG3    DB ' IS: '
DIG     DB ?,'$'

.CODE
MAIN PROC
MOV AX,@DATA
MOV DS,AX

MOV DL,'?'
MOV AH,2
INT 21H
;input
MOV AH,1
INT 21H
MOV DIG1,AL
INT 21H
MOV DIG2,AL
ADD AL,DIG1
SUB AL,30H
MOV DIG,AL
LEA DX,MSG1
MOV AH,9
INT 21H


MOV AH,4CH
INT 21H
MAIN ENDP
        END MAIN

