.model small
.stack 100h
.data
cp db 0ah,0dh, '	1.Capital Letter (A-Z) $'
sm db 0ah,0dh,  '	2.SMALL Letter (a-z) $ '
nm db 0ah,0dh, '	3.NumBer (0-9)$'
msg db 0ah,0dh, '	Give Your Choice 1,2,3 $'
msg1 db 0ah,0dh, '	you select the wrong number $'
.code

main proc

mov ax,@data
mov ds,ax
mov ah,9
lea dx,cp
int 21h

lea dx,sm
int 21h

lea dx,nm
int 21h

lea dx,msg
int 21h

mov ah,2
mov dl,0ah
int 21h
mov dl,0dh
int 21h

mov ah,1
int 21h

mov ah,2
cmp al,'1'
mov dl,'A'
je CL_
cmp al,'2'
mov dl,'a'
je SL_
cmp al,'3'
mov dl,'0'
je NM_
jmp D_
    CL_:
	int 21h
	inc dl
	cmp dl,'['
	je E_
	jne CL_
       
     SL_: 
	int 21h
	inc dl
	cmp dl,'{'
	je E_
	jne SL_

     NM_:
	int 21h
	inc dl
	cmp dl,':'
	je E_
	jne NM_

      D_:
	mov ah,9
	lea dx,msg1
	int 21h
	jmp E_
      
     E_:
	mov ah,4ch
        int 21h

main endp
end main