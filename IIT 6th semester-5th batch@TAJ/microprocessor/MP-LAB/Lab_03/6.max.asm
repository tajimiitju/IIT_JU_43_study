.model small
.stack 100h
.data
msg db 'Input three character to find max character $'
msg1 db 0ah,0dh, 'The Max character is: $'
.code

main proc

mov ax,@data
mov ds,ax
mov ah,9
lea dx,msg
int 21h

mov ah,1
int 21h
mov bl,al

int 21h
mov bh,al

int 21h
mov ch,al

mov ah,9
lea dx,msg1
int 21h

mov dl,bh
mov al,ch
cmp bl,dl
jnge A_
cmp bl,al
jnge B_
mov ah,2
mov dl,bl
int 21h
jmp D_
	A_:
	   mov ah,2
	   cmp dl,al
	   jnge B_
	   int 21h
	   jmp D_
	B_:
	   mov ah,2
	   mov dl,al
	   int 21h
	   jmp D_
	D_:
	   mov ah,4ch
	   int 21h

main endp
end main