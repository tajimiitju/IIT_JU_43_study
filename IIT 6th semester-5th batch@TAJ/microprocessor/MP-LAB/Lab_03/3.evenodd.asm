.model small
.stack 100h
.data
msg0 db 'Please Enter the number you want to check below with two digit 99: $'
msg1 db 0ah,0dh,'The number is even  $' 
msg2 db 0ah,0dh,'The Number is odd  $'


.code
main proc
;initialize data segment 
mov ax,@data;mov data into ax from data segment 
mov ds,ax  	; initialize ds

; print user promt 
lea dx,msg0;
mov ah,9h;
int 21h

;input a chracter name from keyboard
mov ah,1 ; read a character function 
int 21h ; store the first character into al(by default store in ax register)
int 21h	;store the second character into al(by default store in ax register)
cwd ; convert word to double word it is required if the divisor is much smaller than dividend
mov bx,2 ; load 2 into bx 
div bx	; by default after (in word form) div (division) operation the quotient is in ax and remainder is in dx 

;if operation started
	cmp dx,0h ; compare (cmp) dx (remainder) with 0
	je	then_ ; jump if equal to 0h to then_ section in one word go to then_ section
	jne else_ ;jump if not equal to 0h

	
;then_ section is started if after comparison with dx is true
then_:
	lea dx,msg1; load the msg1 meaage from dx
	mov ah,9 ; display the string function 
	int 21h; display msg 1
	jmp end_if ; end_if that means ending the if control instructure not to perform else_section
;else_ section is started if the condition is false 
else_:
	lea dx,msg2; load the msg1 meaage from dx
	mov ah,9 ; display the string function 
	int 21h; display msg 1	
end_if:	; ending the if instructions
	
;dos exit section
mov ah, 4ch ; load 4ch into ah that say ah register to terminate the program 
int 21h	;execute the instructions that 4ch(function number) means

main endp
end main