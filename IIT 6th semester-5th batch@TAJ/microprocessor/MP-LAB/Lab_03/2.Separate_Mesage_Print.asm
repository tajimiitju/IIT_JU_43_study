.model small
.stack 100h
.data
msg db " Hello $"
msg1 db 0ah,0dh, " Bangladesh $"
.code

main proc

mov ax,@data
mov ds,ax
lea dx,msg		;load message
mov ah,9
int 21h	 

lea dx,msg1		;load message
int 21h	

mov ah,4ch
int 21h

main endp
end main