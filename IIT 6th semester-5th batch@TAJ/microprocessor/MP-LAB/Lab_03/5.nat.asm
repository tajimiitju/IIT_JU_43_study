.model small
.stack 100h
.data
msg1 db 'Bangladesh $'
.code

main proc

	mov ax,@data
	mov ds,ax
	
	mov ah,9
    lea dx,msg1
	mov cx,5
TOP:
 	int 21h
 	
 	mov ah,2
 	mov dl,0ah
 	int 21h
 	mov dl,0dh
 	int 21h    
 	mov ah,9
 	lea dx,msg1
 	
  LOOP TOP	

mov ah,4ch
int 21h

main endp
end main