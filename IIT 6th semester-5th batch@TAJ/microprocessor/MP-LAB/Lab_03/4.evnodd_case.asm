; a case example that show 
; 1,3:dispaly o
;  2,4: dispaly e

.model small
.stack 100h
.data
.code

main proc
    
    mov ah,1
    int 21h
    
    
	;1,3 case:

  	cmp al,'1'	;al=1?
	je odd	;yes , dispaly 'o'
	cmp al,'3'	;al=3?
	je odd	;yes , dispaly 'o'
 
    ;2,4 case:
    
	cmp al,'2'	;al=2?
	je even	;yes , dispaly 'e'
	cmp al,'4'	;al=4?
	je even	;yes , dispaly 'e'
	jmp END_

	odd:
	 	 mov dl,'o'
      	 jmp display
	
	even: 
	 	 mov dl,'e'
	
	display:
	 mov ah,2
	 int 21h
    END_:
	mov ah,4ch
 	int 21h

main endp
end main