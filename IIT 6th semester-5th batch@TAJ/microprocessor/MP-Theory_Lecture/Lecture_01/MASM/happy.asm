.MODEL SMALL
.STACK 100H
.DATA
  myMsg DB 0AH,0DH,'WELCOME TO ASSEMBLY LANGUAGE $'
  .CODE
  main PROC
     ;initialize DS
     MOV AX,@DATA
     MOV DS,AX

     LEA DX,myMsg
     MOV AH,9
     INT 21H
     INT 21H
     INT 21H

     MOV AH,4ch
     INT 21H

     main ENDP
     END main
