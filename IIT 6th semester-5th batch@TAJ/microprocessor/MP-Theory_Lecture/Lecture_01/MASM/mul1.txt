MODEL SMALL
.STACK 100
.DATA   ; Data Segment to initialize the variables
Dividend DW 0FFFFH, 0FF88H   ; Dividend = (-78H) = FFFF FF88H (2'Compliment form)
Divisor DW 0006H    ; Divisor = 0006H
Quotient DW ?       ; Variable Quotient to store Quotient
Reminder DW ?       ; Variable Reminder to store Reminder

.CODE
START:
MOV AX,@DATA
MOV DS,AX          ;Initialize data segment
MOV SI,0000H       ;Initialize SI to 0000H
MOV DX,Dividend[SI]    ;Take higher 16-bit number which is to be divided in DX register
MOV AX,Dividend[SI+2]  ;Take lower 16-bit number which is to be divided in AX register
MOV CX,Divisor         ;Take divisor in CX register
IDIV CX                ; Performs signed Division DX:AX � CX
; AX = Quotient DX = Reminder
MOV Reminder,DX         ;Store Reminder
MOV Quotient,AX         ;Store Quotient
INT 03H

END START

