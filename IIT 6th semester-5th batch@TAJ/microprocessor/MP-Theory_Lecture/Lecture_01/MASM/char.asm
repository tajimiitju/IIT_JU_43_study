
;Read a char from keyboard and display it at the new line



.MODEL SMALL
.STACK 100h
.CODE
  
  
  main proc
  	
  	;Display prompt
  	mov ah,2	;
  	mov DL,'?'
  	int 21h
  	
  	;input a char
  	mov ah,1
  	int 21h
  	mov bl,al
  	
  	;a new line
       mov ah,2
        mov dl,0Ah
        int 21h
  	
  	;beginning of the line
        mov dl,0Dh
        int 21h
  	
  	;beep
        mov dl,07h
        int 21h
  	
  	;display char
  	mov dl,bl
  	int 21h
  	
  	;returns to dos
  	mov ah,4ch
  	int 21h 
  	
  	main endp
  	end main
  	
  	
