TITLE PGM4_4: ADD TWO NUMBER
.MODEL SMALL
.STACK 100H
.DATA
       CR    EQU   0DH
       LF    EQU   0AH
MSG1   DB   'Enter your first number: $'
MSG2   DB   'Enter your second number: $'
MSG3   DB   0DH,0AH,'Sumation of two number: '
SUM    DB   ?,'$'
.CODE
MAIN PROC
;initialize DS
         MOV AX,@DATA
         MOV DS,AX
;print user prompt
         LEA DX,MSG1
         MOV AH,9
         INT 21H        
;input your first number
         MOV AH,1             
         INT 21H 
         MOV SUM,AL
;go to a new line
        MOV AH,2       
        MOV DL,0DH        
        INT 21H         
        MOV DL,0AH    
        INT 21H 
;print user prompt
         LEA DX,MSG2
         MOV AH,9
         INT 21H 
;input your second number
         MOV AH,1
         INT 21H
         ADD AL,SUM
         SUB AL,30H
         MOV SUM,AL 
;print user prompt
         LEA DX,MSG3
         MOV AH,9
         INT 21H                  
;RETURN TO DOS
        MOV AX,4C00H        
        INT 21H           
MAIN ENDP
           END MAIN
