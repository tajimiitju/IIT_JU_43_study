TITLE PGM4_1: ECHO PROGRAM
.MODEL SMALL
.STACK 100H
.CODE
MAIN PROC

; DISPLAY FUNCTION

         MOV AH,2                                 ;DISPLAY CHARACTER FUNCTION
         MOV DL,'?'                                ;CHARACTER IS '?'
         INT 21H                                      ;SAVE IT IN BL 

;INPUT A CHARACTER

         MOV AH,1                                 ;READ CHARACTER FUNCTION    
         INT 21H                                     ;CHARACTER IN AL
         MOV BL,AL                               ;SAVE IT BL

;GO TO NEW LINE
        MOV AH,2                                ;DISPLAY CHARACTER FUNCTION
        MOV DL,0DH                           ;CARRIAGE RETURN
        INT 21H                                    ;EXECUTIVE CARRIAGE RETURN
        MOV DL,0AH                           ;LINE FEED
        INT 21H                                    ;EXECUTE LINE FEED

;DISPLSY CHARACTER
        MOV DL,BL                             ;RETRIVE CHARACTER
        INT 21H                                   ;AND DISPLAY IT

;RETURN TO DOS
        MOV AH,4CH                        ;DOS EXIT FUCTION
        INT 21H                                 ;EXIT TO DOS

MAIN ENDP
           END MAIN
