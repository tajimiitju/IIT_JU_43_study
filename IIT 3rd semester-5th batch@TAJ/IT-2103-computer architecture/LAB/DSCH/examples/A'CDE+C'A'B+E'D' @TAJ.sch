DSCH3
VERSION 6/16/2015 12:02:38 AM
BB(-160,-115,88,70)
SYM  #pmos
BB(-75,-75,-55,-55)
TITLE -70 -60  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                               
REC(-75,-75,19,15,r)
VIS 0
PIN(-75,-55,0.000,0.000)s
PIN(-55,-65,0.000,0.000)g
PIN(-75,-75,0.030,0.420)d
LIG(-55,-65,-61,-65)
LIG(-63,-65,-63,-65)
LIG(-65,-71,-65,-59)
LIG(-67,-71,-67,-59)
LIG(-75,-59,-67,-59)
LIG(-75,-55,-75,-59)
LIG(-75,-71,-67,-71)
LIG(-75,-75,-75,-71)
VLG   pmos pmos(drain,source,gate);
FSYM
SYM  #pmos
BB(-75,-95,-55,-75)
TITLE -70 -80  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                               
REC(-75,-95,19,15,r)
VIS 0
PIN(-75,-75,0.000,0.000)s
PIN(-55,-85,0.000,0.000)g
PIN(-75,-95,0.030,0.280)d
LIG(-55,-85,-61,-85)
LIG(-63,-85,-63,-85)
LIG(-65,-91,-65,-79)
LIG(-67,-91,-67,-79)
LIG(-75,-79,-67,-79)
LIG(-75,-75,-75,-79)
LIG(-75,-91,-67,-91)
LIG(-75,-95,-75,-91)
VLG   pmos pmos(drain,source,gate);
FSYM
SYM  #pmos
BB(-45,-95,-25,-75)
TITLE -30 -90  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                               
REC(-44,-90,19,15,r)
VIS 0
PIN(-25,-95,0.000,0.000)s
PIN(-45,-85,0.000,0.000)g
PIN(-25,-75,0.030,0.420)d
LIG(-45,-85,-39,-85)
LIG(-37,-85,-37,-85)
LIG(-35,-79,-35,-91)
LIG(-33,-79,-33,-91)
LIG(-25,-91,-33,-91)
LIG(-25,-95,-25,-91)
LIG(-25,-79,-33,-79)
LIG(-25,-75,-25,-79)
VLG   pmos pmos(drain,source,gate);
FSYM
SYM  #pmos
BB(-25,-95,-5,-75)
TITLE -20 -80  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                               
REC(-25,-95,19,15,r)
VIS 0
PIN(-25,-75,0.000,0.000)s
PIN(-5,-85,0.000,0.000)g
PIN(-25,-95,0.030,0.280)d
LIG(-5,-85,-11,-85)
LIG(-13,-85,-13,-85)
LIG(-15,-91,-15,-79)
LIG(-17,-91,-17,-79)
LIG(-25,-79,-17,-79)
LIG(-25,-75,-25,-79)
LIG(-25,-91,-17,-91)
LIG(-25,-95,-25,-91)
VLG   pmos pmos(drain,source,gate);
FSYM
SYM  #inv
BB(-120,-105,-85,-85)
TITLE -105 -95  #~
MODEL 101
PROP                                                                                                                                    
REC(10,-10,0,0, )
VIS 0
PIN(-120,-95,0.000,0.000)in
PIN(-85,-95,0.030,0.280)out
LIG(-120,-95,-110,-95)
LIG(-110,-105,-110,-85)
LIG(-110,-105,-95,-95)
LIG(-110,-85,-95,-95)
LIG(-93,-95,-93,-95)
LIG(-91,-95,-85,-95)
VLG   not not1(out,in);
FSYM
SYM  #pmos
BB(-30,-75,-10,-55)
TITLE -25 -60  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                               
REC(-30,-75,19,15,r)
VIS 0
PIN(-30,-55,0.000,0.000)s
PIN(-10,-65,0.000,0.000)g
PIN(-30,-75,0.030,0.420)d
LIG(-10,-65,-16,-65)
LIG(-18,-65,-18,-65)
LIG(-20,-71,-20,-59)
LIG(-22,-71,-22,-59)
LIG(-30,-59,-22,-59)
LIG(-30,-55,-30,-59)
LIG(-30,-71,-22,-71)
LIG(-30,-75,-30,-71)
VLG   pmos pmos(drain,source,gate);
FSYM
SYM  #vdd
BB(-80,-105,-70,-95)
TITLE -77 -99  #vdd
MODEL 1
PROP                                                                                                                                    
REC(0,0,0,0, )
VIS 0
PIN(-75,-95,0.000,0.000)vdd
LIG(-75,-95,-75,-100)
LIG(-75,-100,-80,-100)
LIG(-80,-100,-75,-105)
LIG(-75,-105,-70,-100)
LIG(-70,-100,-75,-100)
FSYM
SYM  #pmos
BB(-95,-95,-75,-75)
TITLE -80 -90  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                               
REC(-94,-90,19,15,r)
VIS 0
PIN(-75,-95,0.000,0.000)s
PIN(-95,-85,0.000,0.000)g
PIN(-75,-75,0.030,0.420)d
LIG(-95,-85,-89,-85)
LIG(-87,-85,-87,-85)
LIG(-85,-79,-85,-91)
LIG(-83,-79,-83,-91)
LIG(-75,-91,-83,-91)
LIG(-75,-95,-75,-91)
LIG(-75,-79,-83,-79)
LIG(-75,-75,-75,-79)
VLG   pmos pmos(drain,source,gate);
FSYM
SYM  #vss
BB(-35,62,-25,70)
TITLE -31 67  #vss
MODEL 0
PROP                                                                                                                                    
REC(-35,60,0,0,b)
VIS 0
PIN(-30,60,0.000,0.000)vss
LIG(-30,60,-30,65)
LIG(-35,65,-25,65)
LIG(-35,68,-33,65)
LIG(-33,68,-31,65)
LIG(-31,68,-29,65)
LIG(-29,68,-27,65)
FSYM
SYM  #inv
BB(-5,-55,30,-35)
TITLE 15 -45  #~
MODEL 101
PROP                                                                                                                                    
REC(65,-85,0,0, )
VIS 0
PIN(30,-45,0.000,0.000)in
PIN(-5,-45,0.030,0.140)out
LIG(30,-45,20,-45)
LIG(20,-35,20,-55)
LIG(20,-35,5,-45)
LIG(20,-55,5,-45)
LIG(3,-45,3,-45)
LIG(1,-45,-5,-45)
VLG   not not1(out,in);
FSYM
SYM  #pmos
BB(-45,-55,-25,-35)
TITLE -30 -50  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                               
REC(-44,-50,19,15,r)
VIS 0
PIN(-25,-55,0.000,0.000)s
PIN(-45,-45,0.000,0.000)g
PIN(-25,-35,0.030,0.350)d
LIG(-45,-45,-39,-45)
LIG(-37,-45,-37,-45)
LIG(-35,-39,-35,-51)
LIG(-33,-39,-33,-51)
LIG(-25,-51,-33,-51)
LIG(-25,-55,-25,-51)
LIG(-25,-39,-33,-39)
LIG(-25,-35,-25,-39)
VLG   pmos pmos(drain,source,gate);
FSYM
SYM  #pmos
BB(-25,-55,-5,-35)
TITLE -20 -40  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                               
REC(-25,-55,19,15,r)
VIS 0
PIN(-25,-35,0.000,0.000)s
PIN(-5,-45,0.000,0.000)g
PIN(-25,-55,0.030,0.280)d
LIG(-5,-45,-11,-45)
LIG(-13,-45,-13,-45)
LIG(-15,-51,-15,-39)
LIG(-17,-51,-17,-39)
LIG(-25,-39,-17,-39)
LIG(-25,-35,-25,-39)
LIG(-25,-51,-17,-51)
LIG(-25,-55,-25,-51)
VLG   pmos pmos(drain,source,gate);
FSYM
SYM  #inv
BB(-130,-65,-110,-30)
TITLE -120 -45  #~
MODEL 101
PROP                                                                                                                                    
REC(-65,-175,0,0, )
VIS 0
PIN(-120,-30,0.000,0.000)in
PIN(-120,-65,0.030,0.140)out
LIG(-120,-30,-120,-40)
LIG(-130,-40,-110,-40)
LIG(-130,-40,-120,-55)
LIG(-110,-40,-120,-55)
LIG(-120,-57,-120,-57)
LIG(-120,-59,-120,-65)
VLG   not not1(out,in);
FSYM
SYM  #clock
BB(-150,-48,-135,-42)
TITLE -145 -45  #clock5
MODEL 69
PROP   160.000 160.000                                                                                                                                
REC(-148,-47,6,4,r)
VIS 1
PIN(-135,-45,1.500,0.210)E
LIG(-140,-45,-135,-45)
LIG(-145,-47,-147,-47)
LIG(-141,-47,-143,-47)
LIG(-140,-48,-140,-42)
LIG(-150,-42,-150,-48)
LIG(-145,-43,-145,-47)
LIG(-143,-47,-143,-43)
LIG(-143,-43,-145,-43)
LIG(-147,-43,-149,-43)
LIG(-147,-47,-147,-43)
LIG(-140,-42,-150,-42)
LIG(-140,-48,-150,-48)
FSYM
SYM  #pmos
BB(-95,-75,-75,-55)
TITLE -80 -70  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                               
REC(-94,-70,19,15,r)
VIS 0
PIN(-75,-75,0.000,0.000)s
PIN(-95,-65,0.000,0.000)g
PIN(-75,-55,0.030,0.280)d
LIG(-95,-65,-89,-65)
LIG(-87,-65,-87,-65)
LIG(-85,-59,-85,-71)
LIG(-83,-59,-83,-71)
LIG(-75,-71,-83,-71)
LIG(-75,-75,-75,-71)
LIG(-75,-59,-83,-59)
LIG(-75,-55,-75,-59)
VLG   pmos pmos(drain,source,gate);
FSYM
SYM  #nmos
BB(-5,15,15,35)
TITLE 0 30  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                               
REC(-5,15,19,15,r)
VIS 0
PIN(-5,15,0.000,0.000)s
PIN(15,25,0.000,0.000)g
PIN(-5,35,0.030,0.210)d
LIG(5,25,15,25)
LIG(5,19,5,31)
LIG(3,19,3,31)
LIG(-5,31,3,31)
LIG(-5,35,-5,31)
LIG(-5,19,3,19)
LIG(-5,15,-5,19)
VLG   nmos nmos(drain,source,gate);
FSYM
SYM  #nmos
BB(-5,-5,15,15)
TITLE 0 10  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                               
REC(-5,-5,19,15,r)
VIS 0
PIN(-5,-5,0.000,0.000)s
PIN(15,5,0.000,0.000)g
PIN(-5,15,0.030,0.070)d
LIG(5,5,15,5)
LIG(5,-1,5,11)
LIG(3,-1,3,11)
LIG(-5,11,3,11)
LIG(-5,15,-5,11)
LIG(-5,-1,3,-1)
LIG(-5,-5,-5,-1)
VLG   nmos nmos(drain,source,gate);
FSYM
SYM  #inv
BB(-80,-55,-45,-35)
TITLE -65 -45  #~
MODEL 101
PROP                                                                                                                                    
REC(0,0,0,0, )
VIS 0
PIN(-80,-45,0.000,0.000)in
PIN(-45,-45,0.030,0.140)out
LIG(-80,-45,-70,-45)
LIG(-70,-55,-70,-35)
LIG(-70,-55,-55,-45)
LIG(-70,-35,-55,-45)
LIG(-53,-45,-53,-45)
LIG(-51,-45,-45,-45)
VLG   not not1(out,in);
FSYM
SYM  #inv
BB(5,-30,40,-10)
TITLE 20 -20  #~
MODEL 101
PROP                                                                                                                                    
REC(0,0,0,0, )
VIS 0
PIN(5,-20,0.000,0.000)in
PIN(40,-20,0.030,0.070)out
LIG(5,-20,15,-20)
LIG(15,-30,15,-10)
LIG(15,-30,30,-20)
LIG(15,-10,30,-20)
LIG(32,-20,32,-20)
LIG(34,-20,40,-20)
VLG   not not1(out,in);
FSYM
SYM  #light
BB(43,-15,49,-1)
TITLE 45 -1  #light2
MODEL 49
PROP                                                                                                                                    
REC(44,-14,4,4,r)
VIS 1
PIN(45,0,0.000,0.000)out2
LIG(48,-9,48,-14)
LIG(48,-14,47,-15)
LIG(44,-14,44,-9)
LIG(47,-4,47,-7)
LIG(46,-4,49,-4)
LIG(46,-2,48,-4)
LIG(47,-2,49,-4)
LIG(43,-7,49,-7)
LIG(45,-7,45,0)
LIG(43,-9,43,-7)
LIG(49,-9,43,-9)
LIG(49,-7,49,-9)
LIG(45,-15,44,-14)
LIG(47,-15,45,-15)
FSYM
SYM  #nmos
BB(-100,-20,-80,0)
TITLE -85 -15  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                               
REC(-99,-15,19,15,r)
VIS 0
PIN(-80,0,0.000,0.000)s
PIN(-100,-10,0.000,0.000)g
PIN(-80,-20,0.030,0.350)d
LIG(-90,-10,-100,-10)
LIG(-90,-4,-90,-16)
LIG(-88,-4,-88,-16)
LIG(-80,-16,-88,-16)
LIG(-80,-20,-80,-16)
LIG(-80,-4,-88,-4)
LIG(-80,0,-80,-4)
VLG   nmos nmos(drain,source,gate);
FSYM
SYM  #nmos
BB(-100,0,-80,20)
TITLE -85 5  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                               
REC(-99,5,19,15,r)
VIS 0
PIN(-80,20,0.000,0.000)s
PIN(-100,10,0.000,0.000)g
PIN(-80,0,0.030,0.070)d
LIG(-90,10,-100,10)
LIG(-90,16,-90,4)
LIG(-88,16,-88,4)
LIG(-80,4,-88,4)
LIG(-80,0,-80,4)
LIG(-80,16,-88,16)
LIG(-80,20,-80,16)
VLG   nmos nmos(drain,source,gate);
FSYM
SYM  #nmos
BB(-100,20,-80,40)
TITLE -85 25  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                               
REC(-99,25,19,15,r)
VIS 0
PIN(-80,40,0.000,0.000)s
PIN(-100,30,0.000,0.000)g
PIN(-80,20,0.030,0.070)d
LIG(-90,30,-100,30)
LIG(-90,36,-90,24)
LIG(-88,36,-88,24)
LIG(-80,24,-88,24)
LIG(-80,20,-80,24)
LIG(-80,36,-88,36)
LIG(-80,40,-80,36)
VLG   nmos nmos(drain,source,gate);
FSYM
SYM  #nmos
BB(-100,40,-80,60)
TITLE -85 45  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                               
REC(-99,45,19,15,r)
VIS 0
PIN(-80,60,0.000,0.000)s
PIN(-100,50,0.000,0.000)g
PIN(-80,40,0.030,0.070)d
LIG(-90,50,-100,50)
LIG(-90,56,-90,44)
LIG(-88,56,-88,44)
LIG(-80,44,-88,44)
LIG(-80,40,-80,44)
LIG(-80,56,-88,56)
LIG(-80,60,-80,56)
VLG   nmos nmos(drain,source,gate);
FSYM
SYM  #nmos
BB(-50,-5,-30,15)
TITLE -35 0  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                               
REC(-49,0,19,15,r)
VIS 0
PIN(-30,15,0.000,0.000)s
PIN(-50,5,0.000,0.000)g
PIN(-30,-5,0.030,0.350)d
LIG(-40,5,-50,5)
LIG(-40,11,-40,-1)
LIG(-38,11,-38,-1)
LIG(-30,-1,-38,-1)
LIG(-30,-5,-30,-1)
LIG(-30,11,-38,11)
LIG(-30,15,-30,11)
VLG   nmos nmos(drain,source,gate);
FSYM
SYM  #nmos
BB(-50,15,-30,35)
TITLE -35 20  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                               
REC(-49,20,19,15,r)
VIS 0
PIN(-30,35,0.000,0.000)s
PIN(-50,25,0.000,0.000)g
PIN(-30,15,0.030,0.070)d
LIG(-40,25,-50,25)
LIG(-40,31,-40,19)
LIG(-38,31,-38,19)
LIG(-30,19,-38,19)
LIG(-30,15,-30,19)
LIG(-30,31,-38,31)
LIG(-30,35,-30,31)
VLG   nmos nmos(drain,source,gate);
FSYM
SYM  #nmos
BB(-30,35,-10,55)
TITLE -25 50  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                               
REC(-30,35,19,15,r)
VIS 0
PIN(-30,35,0.000,0.000)s
PIN(-10,45,0.000,0.000)g
PIN(-30,55,0.030,0.210)d
LIG(-20,45,-10,45)
LIG(-20,39,-20,51)
LIG(-22,39,-22,51)
LIG(-30,51,-22,51)
LIG(-30,55,-30,51)
LIG(-30,39,-22,39)
LIG(-30,35,-30,39)
VLG   nmos nmos(drain,source,gate);
FSYM
SYM  #clock
BB(-150,-98,-135,-92)
TITLE -145 -95  #clock1
MODEL 69
PROP   10.000 10.000                                                                                                                                
REC(-148,-97,6,4,r)
VIS 1
PIN(-135,-95,1.500,0.070)A
LIG(-140,-95,-135,-95)
LIG(-145,-97,-147,-97)
LIG(-141,-97,-143,-97)
LIG(-140,-98,-140,-92)
LIG(-150,-92,-150,-98)
LIG(-145,-93,-145,-97)
LIG(-143,-97,-143,-93)
LIG(-143,-93,-145,-93)
LIG(-147,-93,-149,-93)
LIG(-147,-97,-147,-93)
LIG(-140,-92,-150,-92)
LIG(-140,-98,-150,-98)
FSYM
SYM  #clock
BB(35,-68,50,-62)
TITLE 45 -65  #clock2
MODEL 69
PROP   20.000 20.000                                                                                                                                
REC(42,-67,6,4,r)
VIS 1
PIN(35,-65,1.500,0.140)B
LIG(40,-65,35,-65)
LIG(45,-63,47,-63)
LIG(41,-63,43,-63)
LIG(40,-62,40,-68)
LIG(50,-68,50,-62)
LIG(45,-67,45,-63)
LIG(43,-63,43,-67)
LIG(43,-67,45,-67)
LIG(47,-67,49,-67)
LIG(47,-63,47,-67)
LIG(40,-68,50,-68)
LIG(40,-62,50,-62)
FSYM
SYM  #clock
BB(25,-113,40,-107)
TITLE 35 -110  #clock3
MODEL 69
PROP   40.000 40.000                                                                                                                                
REC(32,-112,6,4,r)
VIS 1
PIN(25,-110,1.500,0.210)C
LIG(30,-110,25,-110)
LIG(35,-108,37,-108)
LIG(31,-108,33,-108)
LIG(30,-107,30,-113)
LIG(40,-113,40,-107)
LIG(35,-112,35,-108)
LIG(33,-108,33,-112)
LIG(33,-112,35,-112)
LIG(37,-112,39,-112)
LIG(37,-108,37,-112)
LIG(30,-113,40,-113)
LIG(30,-107,40,-107)
FSYM
SYM  #clock
BB(30,-103,45,-97)
TITLE 40 -100  #clock4
MODEL 69
PROP   80.000 80.000                                                                                                                                
REC(37,-102,6,4,r)
VIS 1
PIN(30,-100,0.030,0.210)D
LIG(35,-100,30,-100)
LIG(40,-98,42,-98)
LIG(36,-98,38,-98)
LIG(35,-97,35,-103)
LIG(45,-103,45,-97)
LIG(40,-102,40,-98)
LIG(38,-98,38,-102)
LIG(38,-102,40,-102)
LIG(42,-102,44,-102)
LIG(42,-98,42,-102)
LIG(35,-103,45,-103)
LIG(35,-97,45,-97)
FSYM
CNC(-135 -45)
CNC(20 -105)
CNC(-100 -50)
CNC(-25 -20)
CNC(-30 -20)
CNC(-5 -20)
CNC(-30 60)
CNC(-135 -45)
CNC(-30 -100)
CNC(-100 -15)
CNC(-135 -45)
CNC(-135 -45)
CNC(-100 -50)
CNC(-135 -45)
LIG(-85,-95,-95,-85)
LIG(-95,-80,-95,-85)
LIG(-75,-95,-25,-95)
LIG(-75,-75,-25,-75)
LIG(-45,-85,-45,-100)
LIG(-45,-100,-30,-100)
LIG(-105,-80,-95,-80)
LIG(-55,-105,20,-105)
LIG(20,-105,20,-30)
LIG(-120,-30,20,-30)
LIG(-105,-50,-105,-80)
LIG(-55,-85,-55,-105)
LIG(-160,-65,-95,-65)
LIG(-55,-65,-55,-50)
LIG(-105,-50,-100,-50)
LIG(-10,-65,-10,45)
LIG(-75,-55,-25,-55)
LIG(30,-100,30,-45)
LIG(20,-110,25,-110)
LIG(20,-105,20,-110)
LIG(-80,-45,-135,-45)
LIG(-135,-95,-120,-95)
LIG(-25,-35,-25,-20)
LIG(-25,-20,-5,-20)
LIG(-100,-10,-100,-15)
LIG(-100,-50,-55,-50)
LIG(-120,-30,-120,10)
LIG(-100,10,-120,10)
LIG(15,-10,15,5)
LIG(-80,-20,-30,-20)
LIG(-30,-5,-30,-20)
LIG(-30,-20,-25,-20)
LIG(-5,-5,-5,-20)
LIG(-5,-20,5,-20)
LIG(-80,60,-30,60)
LIG(-5,35,-5,60)
LIG(-30,55,-30,60)
LIG(-30,60,-5,60)
LIG(-50,5,-160,5)
LIG(-160,5,-160,-65)
LIG(-135,-45,-140,-45)
LIG(-100,50,-135,50)
LIG(-135,50,-135,-45)
LIG(-5,-25,-5,-45)
LIG(-5,-25,55,-25)
LIG(55,-25,55,25)
LIG(15,25,55,25)
LIG(-45,-45,-45,-10)
LIG(-45,-10,15,-10)
LIG(40,-20,40,0)
LIG(40,0,45,0)
LIG(-100,30,-155,30)
LIG(-155,30,-155,-110)
LIG(-155,-110,-30,-110)
LIG(-30,-110,-30,-100)
LIG(-30,-100,30,-100)
LIG(-100,-15,-100,-50)
LIG(-50,25,-105,25)
LIG(-105,25,-105,-15)
LIG(-105,-15,-100,-15)
LIG(-10,-65,35,-65)
LIG(-5,-85,-5,-115)
LIG(-135,-115,-135,-45)
LIG(-135,-115,-5,-115)
TEXT -5 -72  #B
TEXT -145 -57  #E
TEXT -100 43  #E
TEXT 26 1  #E'
TEXT -53 -92  #C
TEXT -45 -81  #D
TEXT -6 -93  #E
TEXT -147 -106  #A
TEXT -15 35  #B
TEXT -125 -31  #C
TEXT -44 -50  #E'
TEXT -102 -85  #A'
TEXT -96 -60  #C'
TEXT 47 -111  #C
TEXT 48 -101  #D
TEXT 47 -66  #B
TEXT 26 24  #D'
TEXT -99 -17  #A'
TEXT -100 5  #C
TEXT -100 24  #D
TEXT -42 -70  #A'
TEXT -49 -2  #C'
TEXT -49 19  #A'
TEXT -5 -52  #D'
FFIG D:\Software\CODES\DSCH\System\A'CDE+C'A'B+E'D' @TAJ.sch
