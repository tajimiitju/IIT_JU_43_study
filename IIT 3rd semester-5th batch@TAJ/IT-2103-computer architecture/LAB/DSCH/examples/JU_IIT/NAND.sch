DSCH3
VERSION 5/20/2015 1:33:15 PM
BB(20,-25,130,85)
SYM  #pmos
BB(45,-5,65,15)
TITLE 60 0  #pmos
MODEL 902
PROP   4.0u 0.05u MP                                                                                                                              
REC(46,0,19,15,r)
VIS 2
PIN(65,-5,0.000,0.000)s
PIN(45,5,0.000,0.000)g
PIN(65,15,0.030,0.210)d
LIG(45,5,51,5)
LIG(53,5,53,5)
LIG(55,11,55,-1)
LIG(57,11,57,-1)
LIG(65,-1,57,-1)
LIG(65,-5,65,-1)
LIG(65,11,57,11)
LIG(65,15,65,11)
VLG  pmos pmos(drain,source,gate);
FSYM
SYM  #pmos
BB(85,-5,105,15)
TITLE 0 90  #pmos
MODEL 902
PROP   4.0u 0.05u MP                                                                                                                              
REC(85,-5,19,15,r)
VIS 2
PIN(85,15,0.000,0.000)s
PIN(105,5,0.000,0.000)g
PIN(85,-5,0.030,0.140)d
LIG(105,5,99,5)
LIG(97,5,97,5)
LIG(95,-1,95,11)
LIG(93,-1,93,11)
LIG(85,11,93,11)
LIG(85,15,85,11)
LIG(85,-1,93,-1)
LIG(85,-5,85,-1)
VLG  pmos pmos(drain,source,gate);
FSYM
SYM  #nmos
BB(55,25,75,45)
TITLE 70 30  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                              
REC(56,30,19,15,r)
VIS 2
PIN(75,45,0.000,0.000)s
PIN(55,35,0.000,0.000)g
PIN(75,25,0.030,0.210)d
LIG(65,35,55,35)
LIG(65,41,65,29)
LIG(67,41,67,29)
LIG(75,29,67,29)
LIG(75,25,75,29)
LIG(75,41,67,41)
LIG(75,45,75,41)
VLG  nmos nmos(drain,source,gate);
FSYM
SYM  #nmos
BB(75,55,95,75)
TITLE 80 70  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                              
REC(75,55,19,15,r)
VIS 2
PIN(75,55,0.000,0.000)s
PIN(95,65,0.000,0.000)g
PIN(75,75,0.030,0.070)d
LIG(85,65,95,65)
LIG(85,59,85,71)
LIG(83,59,83,71)
LIG(75,71,83,71)
LIG(75,75,75,71)
LIG(75,59,83,59)
LIG(75,55,75,59)
VLG  nmos nmos(drain,source,gate);
FSYM
SYM  #vss
BB(70,77,80,85)
TITLE 74 82  #vss
MODEL 0
PROP                                                                                                                                    
REC(70,75,0,0,b)
VIS 0
PIN(75,75,0.000,0.000)vss
LIG(75,75,75,80)
LIG(70,80,80,80)
LIG(70,83,72,80)
LIG(72,83,74,80)
LIG(74,83,76,80)
LIG(76,83,78,80)
FSYM
SYM  #vdd
BB(70,-25,80,-15)
TITLE 73 -19  #vdd
MODEL 1
PROP                                                                                                                                   
REC(0,0,0,0, )
VIS 0
PIN(75,-15,0.000,0.000)vdd
LIG(75,-15,75,-20)
LIG(75,-20,70,-20)
LIG(70,-20,75,-25)
LIG(75,-25,80,-20)
LIG(80,-20,75,-20)
FSYM
SYM  #clock
BB(115,22,130,28)
TITLE 125 25  #clock2
MODEL 69
PROP   20.00 20.00                                                                                                                               
REC(122,23,6,4,r)
VIS 1
PIN(115,25,1.500,0.140)clk2
LIG(120,25,115,25)
LIG(125,27,127,27)
LIG(121,27,123,27)
LIG(120,28,120,22)
LIG(130,22,130,28)
LIG(125,23,125,27)
LIG(123,27,123,23)
LIG(123,23,125,23)
LIG(127,23,129,23)
LIG(127,27,127,23)
LIG(120,22,130,22)
LIG(120,28,130,28)
FSYM
SYM  #light
BB(93,20,99,34)
TITLE 95 34  #light1
MODEL 49
PROP                                                                                                                                   
REC(94,21,4,4,r)
VIS 1
PIN(95,35,0.000,0.000)out1
LIG(98,26,98,21)
LIG(98,21,97,20)
LIG(94,21,94,26)
LIG(97,31,97,28)
LIG(96,31,99,31)
LIG(96,33,98,31)
LIG(97,33,99,31)
LIG(93,28,99,28)
LIG(95,28,95,35)
LIG(93,26,93,28)
LIG(99,26,93,26)
LIG(99,28,99,26)
LIG(95,20,94,21)
LIG(97,20,95,20)
FSYM
SYM  #clock
BB(20,17,35,23)
TITLE 25 20  #clock1
MODEL 69
PROP   10.00 10.00                                                                                                                               
REC(22,18,6,4,r)
VIS 1
PIN(35,20,1.500,0.140)clk1
LIG(30,20,35,20)
LIG(25,18,23,18)
LIG(29,18,27,18)
LIG(30,17,30,23)
LIG(20,23,20,17)
LIG(25,22,25,18)
LIG(27,18,27,22)
LIG(27,22,25,22)
LIG(23,22,21,22)
LIG(23,18,23,22)
LIG(30,23,20,23)
LIG(30,17,20,17)
FSYM
CNC(75 15)
CNC(75 -5)
CNC(105 25)
CNC(45 20)
LIG(65,-5,75,-5)
LIG(65,15,75,15)
LIG(75,25,75,15)
LIG(75,15,85,15)
LIG(75,45,75,55)
LIG(75,-5,75,-15)
LIG(75,-5,85,-5)
LIG(45,5,50,5)
LIG(85,35,95,35)
LIG(45,35,55,35)
LIG(105,5,105,25)
LIG(45,5,45,20)
LIG(95,65,105,65)
LIG(85,25,85,35)
LIG(45,20,45,35)
LIG(105,25,115,25)
LIG(105,25,105,65)
LIG(75,25,85,25)
LIG(35,20,45,20)
FFIG C:\Users\TSS\Desktop\EEE 4307 Digital electronics\DSCH\examples\JU_IIT\NAND.sch
