DSCH3
VERSION 5/26/2015 3:57:44 PM
BB(70,-15,139,20)
SYM  #light
BB(133,-5,139,9)
TITLE 135 9  #light1
MODEL 49
PROP                                                                                                                                    
REC(134,-4,4,4,r)
VIS 1
PIN(135,10,0.000,0.000)out1
LIG(138,1,138,-4)
LIG(138,-4,137,-5)
LIG(134,-4,134,1)
LIG(137,6,137,3)
LIG(136,6,139,6)
LIG(136,8,138,6)
LIG(137,8,139,6)
LIG(133,3,139,3)
LIG(135,3,135,10)
LIG(133,1,133,3)
LIG(139,1,133,1)
LIG(139,3,139,1)
LIG(135,-5,134,-4)
LIG(137,-5,135,-5)
FSYM
SYM  #clock
BB(70,7,85,13)
TITLE 75 10  #clock1
MODEL 69
PROP   10.00 10.00                                                                                                                                
REC(72,8,6,4,r)
VIS 1
PIN(85,10,1.500,0.070)clk1
LIG(80,10,85,10)
LIG(75,8,73,8)
LIG(79,8,77,8)
LIG(80,7,80,13)
LIG(70,13,70,7)
LIG(75,12,75,8)
LIG(77,8,77,12)
LIG(77,12,75,12)
LIG(73,12,71,12)
LIG(73,8,73,12)
LIG(80,13,70,13)
LIG(80,7,70,7)
FSYM
SYM  #bufif1
BB(90,0,125,20)
TITLE 105 10  #1
MODEL 131
PROP                                                                                                                                    
REC(0,5,0,0, )
VIS 5
PIN(90,10,0.000,0.000)in
PIN(105,-5,0.000,0.000)en
PIN(125,10,0.030,0.070)out
LIG(90,10,100,10)
LIG(100,0,100,20)
LIG(100,0,115,10)
LIG(100,20,115,10)
LIG(115,10,125,10)
LIG(105,-5,105,4)
VLG   bufif1 bufif1(out,in,en);
FSYM
SYM  #vdd
BB(100,-15,110,-5)
TITLE 103 -9  #vdd
MODEL 1
PROP                                                                                                                                    
REC(0,0,0,0, )
VIS 0
PIN(105,-5,0.000,0.000)vdd
LIG(105,-5,105,-10)
LIG(105,-10,100,-10)
LIG(100,-10,105,-15)
LIG(105,-15,110,-10)
LIG(110,-10,105,-10)
FSYM
LIG(125,10,135,10)
LIG(85,10,90,10)
FFIG C:\Users\TSS\Desktop\EEE 4307 Digital electronics\DSCH\examples\JU_IIT\buffer.sch
