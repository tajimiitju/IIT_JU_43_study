DSCH3
VERSION 5/20/2015 1:39:03 PM
BB(25,-30,115,65)
SYM  #pmos
BB(55,5,75,25)
TITLE 70 10  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                              
REC(56,10,19,15,r)
VIS 2
PIN(75,5,0.000,0.000)s
PIN(55,15,0.000,0.000)g
PIN(75,25,0.030,0.210)d
LIG(55,15,61,15)
LIG(63,15,63,15)
LIG(65,21,65,9)
LIG(67,21,67,9)
LIG(75,9,67,9)
LIG(75,5,75,9)
LIG(75,21,67,21)
LIG(75,25,75,21)
VLG  pmos pmos(drain,source,gate);
FSYM
SYM  #pmos
BB(75,-20,95,0)
TITLE 80 -5  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                              
REC(75,-20,19,15,r)
VIS 2
PIN(75,0,0.000,0.000)s
PIN(95,-10,0.000,0.000)g
PIN(75,-20,0.030,0.070)d
LIG(95,-10,89,-10)
LIG(87,-10,87,-10)
LIG(85,-16,85,-4)
LIG(83,-16,83,-4)
LIG(75,-4,83,-4)
LIG(75,0,75,-4)
LIG(75,-16,83,-16)
LIG(75,-20,75,-16)
VLG  pmos pmos(drain,source,gate);
FSYM
SYM  #nmos
BB(45,35,65,55)
TITLE 60 40  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                              
REC(46,40,19,15,r)
VIS 2
PIN(65,55,0.000,0.000)s
PIN(45,45,0.000,0.000)g
PIN(65,35,0.030,0.210)d
LIG(55,45,45,45)
LIG(55,51,55,39)
LIG(57,51,57,39)
LIG(65,39,57,39)
LIG(65,35,65,39)
LIG(65,51,57,51)
LIG(65,55,65,51)
VLG  nmos nmos(drain,source,gate);
FSYM
SYM  #nmos
BB(85,35,105,55)
TITLE 90 50  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                              
REC(85,35,19,15,r)
VIS 2
PIN(85,35,0.000,0.000)s
PIN(105,45,0.000,0.000)g
PIN(85,55,0.030,0.140)d
LIG(95,45,105,45)
LIG(95,39,95,51)
LIG(93,39,93,51)
LIG(85,51,93,51)
LIG(85,55,85,51)
LIG(85,39,93,39)
LIG(85,35,85,39)
VLG  nmos nmos(drain,source,gate);
FSYM
SYM  #light
BB(83,15,89,29)
TITLE 85 29  #light1
MODEL 49
PROP                                                                                                                                   
REC(84,16,4,4,r)
VIS 1
PIN(85,30,0.000,0.000)out1
LIG(88,21,88,16)
LIG(88,16,87,15)
LIG(84,16,84,21)
LIG(87,26,87,23)
LIG(86,26,89,26)
LIG(86,28,88,26)
LIG(87,28,89,26)
LIG(83,23,89,23)
LIG(85,23,85,30)
LIG(83,21,83,23)
LIG(89,21,83,21)
LIG(89,23,89,21)
LIG(85,15,84,16)
LIG(87,15,85,15)
FSYM
SYM  #vdd
BB(70,-30,80,-20)
TITLE 73 -24  #vdd
MODEL 1
PROP                                                                                                                                   
REC(0,0,0,0, )
VIS 0
PIN(75,-20,0.000,0.000)vdd
LIG(75,-20,75,-25)
LIG(75,-25,70,-25)
LIG(70,-25,75,-30)
LIG(75,-30,80,-25)
LIG(80,-25,75,-25)
FSYM
SYM  #vss
BB(70,57,80,65)
TITLE 74 62  #vss
MODEL 0
PROP                                                                                                                                    
REC(70,55,0,0,b)
VIS 0
PIN(75,55,0.000,0.000)vss
LIG(75,55,75,60)
LIG(70,60,80,60)
LIG(70,63,72,60)
LIG(72,63,74,60)
LIG(74,63,76,60)
LIG(76,63,78,60)
FSYM
SYM  #clock
BB(25,22,40,28)
TITLE 30 25  #clock2
MODEL 69
PROP   20.00 20.00                                                                                                                               
REC(27,23,6,4,r)
VIS 1
PIN(40,25,1.500,0.140)clk2
LIG(35,25,40,25)
LIG(30,23,28,23)
LIG(34,23,32,23)
LIG(35,22,35,28)
LIG(25,28,25,22)
LIG(30,27,30,23)
LIG(32,23,32,27)
LIG(32,27,30,27)
LIG(28,27,26,27)
LIG(28,23,28,27)
LIG(35,28,25,28)
LIG(35,22,25,22)
FSYM
SYM  #clock
BB(100,12,115,18)
TITLE 110 15  #clock3
MODEL 69
PROP   40.00 40.00                                                                                                                               
REC(107,13,6,4,r)
VIS 1
PIN(100,15,1.500,0.140)clk3
LIG(105,15,100,15)
LIG(110,17,112,17)
LIG(106,17,108,17)
LIG(105,18,105,12)
LIG(115,12,115,18)
LIG(110,13,110,17)
LIG(108,17,108,13)
LIG(108,13,110,13)
LIG(112,13,114,13)
LIG(112,17,112,13)
LIG(105,12,115,12)
LIG(105,18,115,18)
FSYM
CNC(75 35)
CNC(75 30)
LIG(75,0,75,5)
LIG(65,35,75,35)
LIG(65,55,85,55)
LIG(75,25,75,30)
LIG(75,35,85,35)
LIG(95,-10,100,-10)
LIG(100,-10,100,45)
LIG(100,45,105,45)
LIG(55,15,40,15)
LIG(40,45,45,45)
LIG(40,15,40,45)
LIG(75,30,85,30)
LIG(75,30,75,35)
FFIG C:\Users\TSS\Desktop\EEE 4307 Digital electronics\DSCH\examples\JU_IIT\nor.sch
