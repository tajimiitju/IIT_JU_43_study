DSCH3
VERSION 5/20/2015 10:54:44 AM
BB(46,-20,114,50)
SYM  #nmos
BB(70,20,90,40)
TITLE 85 25  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                              
REC(71,25,19,15,r)
VIS 2
PIN(90,40,0.000,0.000)s
PIN(70,30,0.000,0.000)g
PIN(90,20,0.030,0.140)d
LIG(80,30,70,30)
LIG(80,36,80,24)
LIG(82,36,82,24)
LIG(90,24,82,24)
LIG(90,20,90,24)
LIG(90,36,82,36)
LIG(90,40,90,36)
VLG  nmos nmos(drain,source,gate);
FSYM
SYM  #pmos
BB(70,-10,90,10)
TITLE 85 -5  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                              
REC(71,-5,19,15,r)
VIS 2
PIN(90,-10,0.000,0.000)s
PIN(70,0,0.000,0.000)g
PIN(90,10,0.030,0.140)d
LIG(70,0,76,0)
LIG(78,0,78,0)
LIG(80,6,80,-6)
LIG(82,6,82,-6)
LIG(90,-6,82,-6)
LIG(90,-10,90,-6)
LIG(90,6,82,6)
LIG(90,10,90,6)
VLG  pmos pmos(drain,source,gate);
FSYM
SYM  #vdd
BB(85,-20,95,-10)
TITLE 88 -14  #vdd
MODEL 1
PROP                                                                                                                                   
REC(0,0,0,0, )
VIS 0
PIN(90,-10,0.000,0.000)vdd
LIG(90,-10,90,-15)
LIG(90,-15,85,-15)
LIG(85,-15,90,-20)
LIG(90,-20,95,-15)
LIG(95,-15,90,-15)
FSYM
SYM  #vss
BB(85,42,95,50)
TITLE 89 47  #vss
MODEL 0
PROP                                                                                                                                    
REC(85,40,0,0,b)
VIS 0
PIN(90,40,0.000,0.000)vss
LIG(90,40,90,45)
LIG(85,45,95,45)
LIG(85,48,87,45)
LIG(87,48,89,45)
LIG(89,48,91,45)
LIG(91,48,93,45)
FSYM
SYM  #light
BB(108,0,114,14)
TITLE 110 14  #light1
MODEL 49
PROP                                                                                                                                   
REC(109,1,4,4,r)
VIS 1
PIN(110,15,0.000,0.000)out1
LIG(113,6,113,1)
LIG(113,1,112,0)
LIG(109,1,109,6)
LIG(112,11,112,8)
LIG(111,11,114,11)
LIG(111,13,113,11)
LIG(112,13,114,11)
LIG(108,8,114,8)
LIG(110,8,110,15)
LIG(108,6,108,8)
LIG(114,6,108,6)
LIG(114,8,114,6)
LIG(110,0,109,1)
LIG(112,0,110,0)
FSYM
SYM  #button
BB(46,11,55,19)
TITLE 50 15  #button1
MODEL 59
PROP                                                                                                                                   
REC(47,12,6,6,r)
VIS 1
PIN(55,15,0.000,0.000)in1
LIG(54,15,55,15)
LIG(46,19,46,11)
LIG(54,19,46,19)
LIG(54,11,54,19)
LIG(46,11,54,11)
LIG(47,18,47,12)
LIG(53,18,47,18)
LIG(53,12,53,18)
LIG(47,12,53,12)
FSYM
CNC(90 15)
CNC(70 15)
LIG(70,0,70,15)
LIG(90,20,90,15)
LIG(90,15,110,15)
LIG(90,15,90,10)
LIG(55,15,70,15)
LIG(70,15,70,30)
FFIG C:\Users\TSS\Desktop\EEE 4307 Digital electronics\DSCH\examples\JU_IIT\inverter_mono.sch
