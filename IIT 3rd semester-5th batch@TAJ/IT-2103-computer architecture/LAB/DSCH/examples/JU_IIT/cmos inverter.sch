DSCH3
VERSION 5/19/2015 11:41:26 PM
BB(26,-15,104,55)
SYM  #nmos
BB(55,25,75,45)
TITLE 70 30  #nmos
MODEL 901
PROP   1.0u 0.12u MN                                                                                                                              
REC(56,30,19,15,r)
VIS 2
PIN(75,45,0.000,0.000)s
PIN(55,35,0.000,0.000)g
PIN(75,25,0.030,0.140)d
LIG(65,35,55,35)
LIG(65,41,65,29)
LIG(67,41,67,29)
LIG(75,29,67,29)
LIG(75,25,75,29)
LIG(75,41,67,41)
LIG(75,45,75,41)
VLG  nmos nmos(drain,source,gate);
FSYM
SYM  #pmos
BB(55,-5,75,15)
TITLE 70 0  #pmos
MODEL 902
PROP   2.0u 0.12u MP                                                                                                                              
REC(56,0,19,15,r)
VIS 2
PIN(75,-5,0.000,0.000)s
PIN(55,5,0.000,0.000)g
PIN(75,15,0.030,0.140)d
LIG(55,5,61,5)
LIG(63,5,63,5)
LIG(65,11,65,-1)
LIG(67,11,67,-1)
LIG(75,-1,67,-1)
LIG(75,-5,75,-1)
LIG(75,11,67,11)
LIG(75,15,75,11)
VLG  pmos pmos(drain,source,gate);
FSYM
SYM  #light
BB(98,5,104,19)
TITLE 100 19  #light1
MODEL 49
PROP                                                                                                                                   
REC(99,6,4,4,r)
VIS 1
PIN(100,20,0.000,0.000)out1
LIG(103,11,103,6)
LIG(103,6,102,5)
LIG(99,6,99,11)
LIG(102,16,102,13)
LIG(101,16,104,16)
LIG(101,18,103,16)
LIG(102,18,104,16)
LIG(98,13,104,13)
LIG(100,13,100,20)
LIG(98,11,98,13)
LIG(104,11,98,11)
LIG(104,13,104,11)
LIG(100,5,99,6)
LIG(102,5,100,5)
FSYM
SYM  #vdd
BB(70,-15,80,-5)
TITLE 73 -9  #vdd
MODEL 1
PROP                                                                                                                                   
REC(5,5,0,0, )
VIS 0
PIN(75,-5,0.000,0.000)vdd
LIG(75,-5,75,-10)
LIG(75,-10,70,-10)
LIG(70,-10,75,-15)
LIG(75,-15,80,-10)
LIG(80,-10,75,-10)
FSYM
SYM  #vss
BB(70,47,80,55)
TITLE 74 52  #vss
MODEL 0
PROP                                                                                                                                    
REC(70,45,0,0,b)
VIS 0
PIN(75,45,0.000,0.000)vss
LIG(75,45,75,50)
LIG(70,50,80,50)
LIG(70,53,72,50)
LIG(72,53,74,50)
LIG(74,53,76,50)
LIG(76,53,78,50)
FSYM
SYM  #button
BB(26,21,35,29)
TITLE 30 25  #button1
MODEL 59
PROP                                                                                                                                   
REC(27,22,6,6,r)
VIS 1
PIN(35,25,0.000,0.000)in1
LIG(34,25,35,25)
LIG(26,29,26,21)
LIG(34,29,26,29)
LIG(34,21,34,29)
LIG(26,21,34,21)
LIG(27,28,27,22)
LIG(33,28,27,28)
LIG(33,22,33,28)
LIG(27,22,33,22)
FSYM
CNC(75 20)
CNC(55 25)
CNC(55 25)
LIG(55,5,55,25)
LIG(75,25,75,20)
LIG(100,20,75,20)
LIG(75,20,75,15)
LIG(35,25,55,25)
LIG(55,25,55,35)
FFIG C:\Users\TSS\Desktop\EEE 4307 Digital electronics\DSCH\examples\JU_IIT\cmos inverter.sch
