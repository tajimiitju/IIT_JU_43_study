DSCH3
VERSION 5/19/2015 11:45:29 PM
BB(15,-25,135,45)
SYM  #kbd
BB(15,-5,65,35)
TITLE 15 37  #kbd1
MODEL 85
PROP                                                                                                                                   
REC(15,-5,40,40,r)
VIS 4
PIN(65,30,0.000,0.000)kbd1
PIN(65,20,0.000,0.000)kbd2
PIN(65,10,0.000,0.000)kbd3
PIN(65,0,0.000,0.000)kbd4
LIG(55,-5,55,35)
LIG(15,-5,55,-5)
LIG(15,-5,15,35)
LIG(49,0,49,3)
LIG(15,15,55,15)
LIG(35,-5,35,35)
LIG(25,35,25,-5)
LIG(15,5,55,5)
LIG(45,-5,45,35)
LIG(15,25,55,25)
LIG(55,30,65,30)
LIG(65,20,55,20)
LIG(55,10,65,10)
LIG(65,0,55,0)
LIG(19,33,19,27)
LIG(19,27,21,27)
LIG(21,27,21,33)
LIG(21,33,19,33)
LIG(31,33,31,27)
LIG(39,27,41,27)
LIG(41,27,41,29)
LIG(41,29,39,29)
LIG(39,29,39,33)
LIG(39,33,41,33)
LIG(49,33,51,33)
LIG(51,27,49,27)
LIG(51,27,51,33)
LIG(49,29,51,29)
LIG(19,17,19,21)
LIG(19,21,21,21)
LIG(21,17,21,23)
LIG(31,17,29,17)
LIG(29,17,29,19)
LIG(29,19,31,19)
LIG(31,19,31,23)
LIG(31,23,29,23)
LIG(39,17,39,23)
LIG(39,23,41,23)
LIG(41,23,41,19)
LIG(41,19,39,19)
LIG(49,17,51,17)
LIG(51,17,51,23)
LIG(19,7,19,13)
LIG(19,7,21,7)
LIG(21,7,21,13)
LIG(21,13,19,13)
LIG(19,9,21,9)
LIG(49,0,51,0)
LIG(15,35,55,35)
LIG(29,7,31,7)
LIG(49,-3,49,0)
LIG(38,3,38,0)
LIG(38,-3,41,-3)
LIG(29,-3,29,3)
LIG(18,3,18,-3)
LIG(18,-3,21,-3)
LIG(48,7,51,7)
LIG(51,7,52,8)
LIG(52,8,51,9)
LIG(48,9,51,9)
LIG(48,13,48,9)
LIG(29,9,31,9)
LIG(18,3,21,3)
LIG(29,7,29,9)
LIG(31,9,31,13)
LIG(31,7,31,9)
LIG(31,13,29,13)
LIG(38,3,41,3)
LIG(38,13,40,7)
LIG(40,7,42,13)
LIG(48,9,48,7)
LIG(51,9,52,10)
LIG(49,-3,52,-3)
LIG(52,10,52,12)
LIG(38,11,42,11)
LIG(32,2,31,3)
LIG(52,12,51,13)
LIG(38,0,38,-3)
LIG(38,0,40,0)
LIG(32,-2,32,2)
LIG(31,-3,32,-2)
LIG(28,3,29,3)
LIG(28,-3,29,-3)
LIG(48,13,51,13)
LIG(29,-3,31,-3)
LIG(29,3,31,3)
FSYM
SYM  #digit
BB(110,-25,135,10)
TITLE 110 7  #digit1
MODEL 89
PROP                                                                                                                                   
REC(115,-20,15,21,r)
VIS 4
PIN(115,10,0.000,0.000)digit1
PIN(120,10,0.000,0.000)digit2
PIN(125,10,0.000,0.000)digit3
PIN(130,10,0.000,0.000)digit4
LIG(110,-25,110,5)
LIG(135,-25,110,-25)
LIG(135,5,135,-25)
LIG(110,5,135,5)
LIG(115,5,115,10)
LIG(120,5,120,10)
LIG(125,5,125,10)
LIG(130,5,130,10)
FSYM
CNC(130 45)
LIG(65,0,90,0)
LIG(90,0,90,10)
LIG(90,10,115,10)
LIG(65,10,80,10)
LIG(80,10,80,20)
LIG(80,20,120,20)
LIG(120,10,120,20)
LIG(65,20,75,20)
LIG(75,20,75,35)
LIG(75,35,125,35)
LIG(125,10,125,35)
LIG(65,30,65,45)
LIG(65,45,130,45)
LIG(130,10,130,45)
LIG(130,45,135,45)
FFIG C:\Users\TSS\Desktop\EEE 4307 Digital electronics\DSCH\examples\JU_IIT\input_output.sch
