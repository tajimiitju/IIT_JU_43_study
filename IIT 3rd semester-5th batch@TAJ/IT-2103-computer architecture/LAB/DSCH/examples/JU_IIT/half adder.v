// DSCH3
// 5/22/2015 9:07:48 PM
// C:\Users\TSS\Desktop\EEE 4307 Digital electronics\DSCH\examples\JU_IIT\half adder.sch

module halfadder( A,B,S,C);
 input A,B;
 output S,C;
 wire ;
 xor #(16) xor2_1(S,A,B);
 and #(16) and2_2(C,B,A);
endmodule

// Simulation parameters in Verilog Format
always
#1000 A=~A;
#2000 B=~B;

// Simulation parameters
// A CLK 10 10
// B CLK 20 20
