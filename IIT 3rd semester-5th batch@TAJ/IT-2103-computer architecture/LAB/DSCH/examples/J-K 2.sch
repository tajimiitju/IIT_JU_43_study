DSCH3
VERSION 9/13/2015 12:21:56 AM
BB(-5,-35,124,55)
SYM  #nor2
BB(80,-15,115,5)
TITLE 100 -5  #|
MODEL 302
PROP                                                                                                                                   
REC(0,-20,0,0, )
VIS 0
PIN(80,-10,0.000,0.000)a
PIN(80,0,0.000,0.000)b
PIN(115,-5,0.090,0.210)s
LIG(80,0,93,0)
LIG(92,2,88,5)
LIG(107,-3,104,1)
LIG(108,-5,107,-3)
LIG(107,-7,108,-5)
LIG(104,-11,107,-7)
LIG(99,-14,104,-11)
LIG(104,1,99,4)
LIG(99,4,88,5)
LIG(88,-15,99,-14)
LIG(94,-2,92,2)
LIG(88,-15,92,-12)
LIG(92,-12,94,-8)
LIG(94,-8,95,-5)
LIG(95,-5,94,-2)
LIG(80,-10,93,-10)
LIG(112,-5,115,-5)
LIG(110,-5,110,-5)
VLG  nor nor2(s,a,b);
FSYM
SYM  #nor2
BB(80,25,115,45)
TITLE 100 35  #|
MODEL 302
PROP                                                                                                                                   
REC(0,-10,0,0, )
VIS 0
PIN(80,30,0.000,0.000)a
PIN(80,40,0.000,0.000)b
PIN(115,35,0.090,0.210)s
LIG(80,40,93,40)
LIG(92,42,88,45)
LIG(107,37,104,41)
LIG(108,35,107,37)
LIG(107,33,108,35)
LIG(104,29,107,33)
LIG(99,26,104,29)
LIG(104,41,99,44)
LIG(99,44,88,45)
LIG(88,25,99,26)
LIG(94,38,92,42)
LIG(88,25,92,28)
LIG(92,28,94,32)
LIG(94,32,95,35)
LIG(95,35,94,38)
LIG(80,30,93,30)
LIG(112,35,115,35)
LIG(110,35,110,35)
VLG  nor nor2(s,a,b);
FSYM
SYM  #and3
BB(10,-20,45,10)
TITLE 25 -5  #&
MODEL 403
PROP                                                                                                                                   
REC(10,-20,0,0,P)
VIS 0
PIN(10,-15,0.000,0.000)a
PIN(10,-5,0.000,0.000)b
PIN(10,5,0.000,0.000)c
PIN(50,-5,0.090,0.070)s
LIG(50,-5,40,-5)
LIG(10,5,20,5)
LIG(10,-5,20,-5)
LIG(10,-15,20,-15)
LIG(20,-5,20,10)
LIG(35,6,30,9)
LIG(39,-2,35,6)
LIG(20,-20,20,-5)
LIG(20,-20,30,-19)
LIG(40,-5,39,-2)
LIG(39,-8,40,-5)
LIG(20,10,30,9)
LIG(35,-16,39,-8)
LIG(30,-19,35,-16)
VLG  and and3(s,a,b,c);
FSYM
SYM  #and3
BB(10,20,45,50)
TITLE 25 35  #&
MODEL 403
PROP                                                                                                                                   
REC(10,20,0,0,P)
VIS 0
PIN(10,25,0.000,0.000)a
PIN(10,35,0.000,0.000)b
PIN(10,45,0.000,0.000)c
PIN(50,35,0.090,0.070)s
LIG(50,35,40,35)
LIG(10,45,20,45)
LIG(10,35,20,35)
LIG(10,25,20,25)
LIG(20,35,20,50)
LIG(35,46,30,49)
LIG(39,38,35,46)
LIG(20,20,20,35)
LIG(20,20,30,21)
LIG(40,35,39,38)
LIG(39,32,40,35)
LIG(20,50,30,49)
LIG(35,24,39,32)
LIG(30,21,35,24)
VLG  and and3(s,a,b,c);
FSYM
SYM  #light
BB(118,-20,124,-6)
TITLE 120 -6  # Q
MODEL 49
PROP                                                                                                                                   
REC(119,-19,4,4,r)
VIS 1
PIN(120,-5,0.000,0.000) Q
LIG(123,-14,123,-19)
LIG(123,-19,122,-20)
LIG(119,-19,119,-14)
LIG(122,-9,122,-12)
LIG(121,-9,124,-9)
LIG(121,-7,123,-9)
LIG(122,-7,124,-9)
LIG(118,-12,124,-12)
LIG(120,-12,120,-5)
LIG(118,-14,118,-12)
LIG(124,-14,118,-14)
LIG(124,-12,124,-14)
LIG(120,-20,119,-19)
LIG(122,-20,120,-20)
FSYM
SYM  #light
BB(118,20,124,34)
TITLE 120 34  # Q'
MODEL 49
PROP                                                                                                                                   
REC(119,21,4,4,r)
VIS 1
PIN(120,35,0.000,0.000) Q'
LIG(123,26,123,21)
LIG(123,21,122,20)
LIG(119,21,119,26)
LIG(122,31,122,28)
LIG(121,31,124,31)
LIG(121,33,123,31)
LIG(122,33,124,31)
LIG(118,28,124,28)
LIG(120,28,120,35)
LIG(118,26,118,28)
LIG(124,26,118,26)
LIG(124,28,124,26)
LIG(120,20,119,21)
LIG(122,20,120,20)
FSYM
SYM  #clock
BB(-5,-8,10,-2)
TITLE 0 -5  #clock1
MODEL 69
PROP   10.000 10.000                                                                                                                               
REC(-3,-7,6,4,r)
VIS 1
PIN(10,-5,1.500,0.070)K
LIG(5,-5,10,-5)
LIG(0,-7,-2,-7)
LIG(4,-7,2,-7)
LIG(5,-8,5,-2)
LIG(-5,-2,-5,-8)
LIG(0,-3,0,-7)
LIG(2,-7,2,-3)
LIG(2,-3,0,-3)
LIG(-2,-3,-4,-3)
LIG(-2,-7,-2,-3)
LIG(5,-2,-5,-2)
LIG(5,-8,-5,-8)
FSYM
SYM  #clock
BB(-5,12,10,18)
TITLE 0 15  #clock2
MODEL 69
PROP   20.000 20.000                                                                                                                               
REC(-3,13,6,4,r)
VIS 1
PIN(10,15,1.500,0.140)CP
LIG(5,15,10,15)
LIG(0,13,-2,13)
LIG(4,13,2,13)
LIG(5,12,5,18)
LIG(-5,18,-5,12)
LIG(0,17,0,13)
LIG(2,13,2,17)
LIG(2,17,0,17)
LIG(-2,17,-4,17)
LIG(-2,13,-2,17)
LIG(5,18,-5,18)
LIG(5,12,-5,12)
FSYM
SYM  #clock
BB(-5,32,10,38)
TITLE 0 35  #clock3
MODEL 69
PROP   40.000 40.000                                                                                                                               
REC(-3,33,6,4,r)
VIS 1
PIN(10,35,1.500,0.070)J
LIG(5,35,10,35)
LIG(0,33,-2,33)
LIG(4,33,2,33)
LIG(5,32,5,38)
LIG(-5,38,-5,32)
LIG(0,37,0,33)
LIG(2,33,2,37)
LIG(2,37,0,37)
LIG(-2,37,-4,37)
LIG(-2,33,-2,37)
LIG(5,38,-5,38)
LIG(5,32,-5,32)
FSYM
CNC(115 35)
LIG(80,-10,50,-10)
LIG(50,-10,50,-5)
LIG(10,5,10,25)
LIG(115,35,115,55)
LIG(80,0,80,10)
LIG(80,10,115,10)
LIG(115,10,115,35)
LIG(80,30,80,20)
LIG(80,20,120,20)
LIG(120,20,120,-5)
LIG(115,-5,120,-5)
LIG(115,35,120,35)
LIG(50,35,50,40)
LIG(50,40,80,40)
LIG(10,-15,10,-25)
LIG(10,-25,115,-25)
LIG(115,-25,115,-5)
LIG(10,45,10,55)
LIG(10,55,115,55)
TEXT 42 -35  #J-K Flip Flop
FFIG E:\IIT@TAJ\IIT 3rd semester-5th batch@TAJ\IT-2103-computer architecture\LAB\DSCH\examples\J-K 2.sch
