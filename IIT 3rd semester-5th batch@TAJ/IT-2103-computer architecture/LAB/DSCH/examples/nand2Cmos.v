// DSCH3
// 5/19/2015 11:53:47 PM
// C:\Users\TSS\Desktop\EEE 4307 Digital electronics\DSCH\examples\nand2Cmos.sch

module nand2Cmos( B,A,Nand2);
 input B,A;
 output Nand2;
 wire w3,;
 nmos #(107) nmos_1(w3,vss,B); // 1.0u 0.12u
 pmos #(121) pmos_2(Nand2,vdd,B); // 2.0u 0.12u
 pmos #(121) pmos_3(Nand2,vdd,A); // 2.0u 0.12u
 nmos #(121) nmos_4(Nand2,w3,A); // 1.0u 0.12u
endmodule

// Simulation parameters in Verilog Format
always
#1000 B=~B;
#2000 A=~A;

// Simulation parameters
// B CLK 10 10
// A CLK 20 20
