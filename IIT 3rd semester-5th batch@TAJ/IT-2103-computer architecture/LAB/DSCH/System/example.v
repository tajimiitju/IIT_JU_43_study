// DSCH3
// 4/2/2010 10:16:01 PM
// example

module example( in1,in2,in3,in4,in5,in6,in7,in8,
 out1,out2,out3,out4);
 input in1,in2,in3,in4,in5,in6,in7,in8;
 output out1,out2,out3,out4;
 wire ;
 nand #(13) nand2_1(out1,in2,in1);
 nand #(13) nand2_2(out2,in4,in3);
 nand #(13) nand2_3(out4,in7,in8);
 nand #(13) nand2_4(out3,in5,in6);
endmodule

// Simulation parameters in Verilog Format
always
#1000 in1=~in1;
#2000 in2=~in2;
#4000 in3=~in3;
#8000 in4=~in4;
#16000 in5=~in5;
#32000 in6=~in6;
#64000 in7=~in7;
#128000 in8=~in8;

// Simulation parameters
// in1 CLK 10 10
// in2 CLK 20 20
// in3 CLK 40 40
// in4 CLK 80 80
// in5 CLK 160 160
// in6 CLK 320 320
// in7 CLK 640 640
// in8 CLK 1280 1280
