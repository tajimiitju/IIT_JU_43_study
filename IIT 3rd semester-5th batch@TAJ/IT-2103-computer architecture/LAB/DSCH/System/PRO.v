// DSCH3
// 4/2/2010 11:26:47 PM
// E:\Galib\DSCH\System\PRO.sch

module PRO( B1,B2,A4,A3,A2,A1,B4,B3,
 R,Q,P,out1,out2,out3,out4,SF,
 CF,ZF);
 input B1,B2,A4,A3,A2,A1,B4,B3;
 input R,Q,P;
 output out1,out2,out3,out4,SF,CF,ZF;
 wire w2,w3,w4,w5,w6,w7,w8,w9;
 wire w10,w11,w12,w13,w14,w15,w16,w17;
 wire w18,w19,w20,w21,w22,w23,w24,w25;
 wire w26,w27,w28,w29,w30,w31,w32,w33;
 wire w34,w35,w36,w37,w38,w39,w40,w41;
 wire w42,w43,w44,w45,w46,w47,w48,w49;
 wire w50,w51,w52,w53,w54,w55,w56,w57;
 wire w58,w59,w60,w61,w62,w63,w64,w65;
 wire w66,w67,w68,w69,w70,w71,w72,w73;
 wire w74,w75,w76,w77,w78,w79,w80,w81;
 wire w82,w83,w84,w85,w86,w87,w88,w89;
 wire w90,w91,w92,w93,w94,w95,w96,w97;
 wire w98,w99,w100,w101,w102,w103,w104,w105;
 wire w106,w107,w108,w109,w110,w111,w112,w113;
 wire w114,w115,w116;
endmodule

// Simulation parameters in Verilog Format
always
#1000 B1=~B1;
#2000 B2=~B2;
#4000 A4=~A4;
#8000 A3=~A3;
#16000 A2=~A2;
#32000 A1=~A1;
#64000 B4=~B4;
#128000 B3=~B3;
#256000 R=~R;
#512000 Q=~Q;
#1024000 P=~P;

// Simulation parameters
// B1 CLK 10 10
// B2 CLK 20 20
// A4 CLK 40 40
// A3 CLK 80 80
// A2 CLK 160 160
// A1 CLK 320 320
// B4 CLK 640 640
// B3 CLK 1280 1280
// R CLK 2560 2560
// Q CLK 5120 5120
// P CLK 10240 10240
