// DSCH3
// 4/3/2010 10:27:31 AM
// E:\Galib\DSCH\System\mux.sch

module mux( in1,i2,i3,i4,A,B,o1);
 input in1,i2,i3,i4,A,B;
 output o1;
 wire w3,w4,w6,w9,w11,w13,w14,;
 not #(17) inv_1(w3,B);
 and #(16) and3_2(w6,w4,B,i2);
 and #(16) and3_3(w9,A,w3,i3);
 and #(16) and3_4(w11,A,B,i4);
 and #(16) and3_5(w13,w4,w3,in1);
 or #(16) or2_6(o1,w13,w14);
 or #(19) or3_7(w14,w6,w9,w11);
 not #(17) inv_8(w4,A);
endmodule

// Simulation parameters in Verilog Format
always
#1000 in1=~in1;
#2000 i2=~i2;
#4000 i3=~i3;
#8000 i4=~i4;
#16000 A=~A;
#32000 B=~B;

// Simulation parameters
// in1 CLK 10 10
// i2 CLK 20 20
// i3 CLK 40 40
// i4 CLK 80 80
// A CLK 160 160
// B CLK 320 320
