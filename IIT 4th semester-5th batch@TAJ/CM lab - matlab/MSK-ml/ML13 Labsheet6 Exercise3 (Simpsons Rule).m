% MIST Labsheet-6: Exercise-3 (Simpsons Rule)

clc;
clear all;
close all;

x = [1 2 3 4 5 6 7];
y = [2 5 10 17 26 37 50];
h = (x(2)-x(1))/7;

a = (y(1)+y(7)+((y(3)+y(5))*2)+((y(2)+y(4)+y(6))*4))*h/3