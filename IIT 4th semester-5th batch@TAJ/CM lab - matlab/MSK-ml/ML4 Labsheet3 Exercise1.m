% MIST Labsheet-3: Exercise-1 

clc;
close all;
clear all;
x0 = 2;
for i=1:20;
    x = x0-(mlf4(x0)/mlf5(x0));
    if(abs(x-x0)<10^(-5))
        break;
    else
        x0=x;
    end
end
x
i