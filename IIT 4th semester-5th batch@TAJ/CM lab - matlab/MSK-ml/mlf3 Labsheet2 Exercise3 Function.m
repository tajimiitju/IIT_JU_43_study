% MIST Labsheet-2: Exercise-3 Function

function z = mlf3(x);
z = x-exp(-x);