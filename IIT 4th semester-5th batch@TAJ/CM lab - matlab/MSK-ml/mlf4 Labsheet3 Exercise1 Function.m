% MIST Labsheet-3: Exercise-1 Function

function z = mlf4(x);
z = x^(3)-3*x-1;
