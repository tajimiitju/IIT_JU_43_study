% MIST Labsheet-6: Exercise-1 (Simpsons Rule)

clc;
clear all;
close all;
x0 = 0;
xn = pi/2;
ym = 0;
yn = 0;
s = 0;
n = 100;
h = (xn-x0)/n;
% h = pi/4;
% h = pi/8;
for i = x0+h:h:xn-h
    s = s+1;
    if (((-1)^s) > 0)
        y1 = mlf8(i);
        yn = yn+y1;
    else
        y2 = mlf8(i);
        ym = ym+y2;
    end
end

a = (mlf8(x0)+mlf8(xn)+(yn*2)+(ym*4))*h/3