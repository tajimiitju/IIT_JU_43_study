% MIST Labsheet-2 and 3: Exercise-2 Function

function z = mlf2(x);
z = x*(sin(x))+cos(x);