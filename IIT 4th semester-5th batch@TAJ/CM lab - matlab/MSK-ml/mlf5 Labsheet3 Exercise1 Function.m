% MIST Labsheet-3: Exercise-1 Function

function z = mlf5(x);
z = 3*x^(2) - 3;