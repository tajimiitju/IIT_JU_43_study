% MIST Labsheet-3: Exercise-2

clc;
close all;
clear all;
x0 = 3;
for i=1:20;
    x = x0-(mlf2(x0)/mlf6(x0));
    if(abs(x-x0)<10^(-5))
        break;
    else
        x0=x;
    end
end
x
i