% MIST Labsheet-2: Exercise-1 Function

function z = mlf1(x);
z = x^3-2*x-5;