% Lab Test-Set-B: Question-1 Function
 
function z = mlf10(x);
z = 2*(x^4)-4*(x^3)+6*(x^2)-8*x+1;