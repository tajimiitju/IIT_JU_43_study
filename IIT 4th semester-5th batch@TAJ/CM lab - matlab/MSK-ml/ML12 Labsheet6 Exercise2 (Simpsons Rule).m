%MIST Labsheet-6: Exercise-2 (Simpsons Rule)

clc;
clear all;
close all;
x0 = 0;
xn = 1;
ym = 0;
yn = 0;
s = 0;
n = 100;
h = (xn-x0)/n;
h = 0.5;
% h = 0.25;
% h = 0.125;
for i = x0+h:h:xn-h
    s = s+1;
    if (((-1)^s) > 0)
        y1 = mlf9(i);
        yn = yn+y1;
    else
        y2 = mlf9(i);
        ym = ym+y2;
    end
end

a = (mlf9(x0)+mlf9(xn)+(yn*2)+(ym*4))*h/3

