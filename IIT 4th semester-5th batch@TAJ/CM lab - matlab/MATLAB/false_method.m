clc;
clear all;
%here the function
f1=input('Enter the equation:','s');
f=inline(f1);
a=input('enter the 1st boundary value:');
b=input('enter the 2nd boundary value:');
tolerance=10^(-8);
e=2.7182818;
for i=1:10
    x0=a; x1=b;
    fprintf('\n root lies between (%.4f,%.0f)',a,b)
    x2(i)=x0-(f(x0)*(x1-x0)/(f(x1)-f(x0)))
    if f(x2(i))>0
        b=x2(i);
    else
        a=x2(i);
    end
    fprintf('\n therefore, x2=%.4f\n here, f(x2)=%.4f',x2(i),f(x2(i)))
    p=x2(i);
end

for i=1:10
    error(i)=p-x2(i);
end
answer=p;
plot (error)
grid on;
title('plot of error');
xlabel('iteration');
ylabel('error');

%this equation's errors can not be ploted
    
