% Newton Raphson Method
 clear all
 close all
 clc
 % Change here for different functions
 
 f1=input('Enter the equation:','s');
f=inline(f1);
 %this is the derivative of the above function

 f2=input('Enter the derivative of the above function:','s');
df=inline(f2);
 % Change lower limit 'a' and upper limit 'b'
 %a=0; b=1;
 a=input('enter the lower limit:');
b=input('enter the upper limit:');
 x=a;
 c=0;
 %x=1;
 for i=1:1:100
 x1=x-(f(x)/df(x));
 c=c+1;
 x=x1;
 end
 sol=x;
 fprintf('Approximate Root is %.5f',sol)
 fprintf('\nnumber of iteration is %d',c)
 a=0;b=1;
 x=a;
 er(5)=0;
 for i=1:1:5
 x1=x-(f(x)/df(x));
 x=x1;
 er(i)=x1-sol;
 end
 plot(er)
 xlabel('Number of iterations')
 ylabel('Error')
 title('Error Vs. Number of iterations')