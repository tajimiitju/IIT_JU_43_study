function y = f(x)
y = x.^3-2*x-5;
fun = @f; % function
x0 = 2; % initial point
z = fzero(fun,x0)