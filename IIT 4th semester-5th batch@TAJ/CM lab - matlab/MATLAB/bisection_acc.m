clc;
clear all;
close all;

f1=input('Enter the equation:','s');
f=inline(f1);
a=input('enter the 1st boundary value:');
b=input('enter the 2nd boundary value:');

acc=input('enter the accuracy:');

%disp('****************************');

m=(a+b)/2;

i=0;

while abs(f(m))> acc
    i=1+i;
    
    %fprintf('%d)\t%f\tf(x)=%f\n',i,m,f(m));
    if (f(m)*f(b))<0
        a=m;
    else
        b=m;
    end
    
    m=(a+b)/2;
end
%fprintf('%d)\t%f\tf(x)=%f\n',i,m,f(m));
disp('****************************');
fprintf('the root of the equation(accuracy type)=%.4f\n',m);
    